from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from unittest import skip
from workingon.models import Standup


class InputStandupTest(TestCase):

    def setUp(self):
        User.objects.create_user('test', 'test@info.com', 'test')
        self.client.login(username="test", password="test")

    def post_input_standup(self, standup):
        return self.client.post('/standup/',
                                data={'new-standup': standup},
                                HTTP_X_REQUESTED_WITH='XMLHttpRequest')

    def test_for_empty_input_renders_ok(self):
        response = self.post_input_standup('')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Standup.objects.count(), 0)

    def test_saving_a_POST_request(self):
        self.post_input_standup('Team meeting')
        self.assertEqual(Standup.objects.count(), 1)
