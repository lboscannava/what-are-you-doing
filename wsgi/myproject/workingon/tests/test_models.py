from django.test import TestCase
from unittest import skip
from workingon.models import Standup
from django.utils import timezone
from django.core.exceptions import ValidationError


class ListAndSaveModelsTests(TestCase):

    def test_can_save_with_empty_details(self):
        standup = Standup(today="Team meeting")
        standup.save()
        self.assertEqual(Standup.objects.count(), 1)

    def test_cannot_save_empty_standup(self):
        standup = Standup()
        with self.assertRaises(ValidationError):
            standup.save()
            standup.full_clean()

    def test_cannot_save_duplicate_day(self):
        standup1 = Standup(today="Team meeting", date=timezone.now())
        standup1.save()
        standup2 = Standup(today="Another meeting", date=timezone.now())
        with self.assertRaises(ValidationError):
            standup2.save()
            standup2.full_clean()
        self.assertEqual(Standup.objects.count(), 1)
