from django.core.management.base import BaseCommand, CommandError
from workingon.models import Project, Task, UakaUser
from workingon.everhourapi import EverhourSession
from workingon.asanaapi import AsanaSession

class Command(BaseCommand):

	def handle(self, *args, **options):
		projects = Project.objects.all()
		access = self.check_user()
		if access:
			self.update(access)

	def check_user(self):
		for user in UakaUser.objects.all():
			if user.access_token and user.api_key:
				token = user.access_token
				api_key = user.api_key
				return {"api_key": api_key, "access_token": token}
		return None

	def update_tasks(self, project, task_id, token):	
		tasks = AsanaSession.tasks_list(str(task_id), token)
		self.remove_nonexistent_tasks(tasks)
		for task in tasks:
			tmp = Task.objects.filter(project=project, task_name=task['name'])
			if len(tmp) is 0:			
				new_task = Task.objects.create(project=project, task_name=task['name'])
				new_task.save()

	def update(self, access):
		projects = EverhourSession.projects_list(access['api_key'])
		self.remove_nonexistent_projects(projects)		
		for project in projects:
			tmp = Project.objects.filter(project_name="@" + project['name'])	
			if len(tmp) is 0:			
				new_project = Project.objects.create(project_name="@" + project['name'], project_id=project['id'])
				new_project.save()
				self.update_tasks(new_project, project['id'], access['access_token'])
			else:
				self.update_tasks(tmp[0], tmp[0].project_id, access['access_token'])

	def remove_nonexistent_projects(self, projects):
		db_projects = Project.objects.all()		
		for db_project in db_projects:
			id_project = db_project.project_id
			for project in projects:
				if int(project['id']) == int(id_project):
					continue
				else:
					Project.objects.filter(project_name=db_project.project_name).delete()
			

	def remove_nonexistent_tasks(self, tasks):
		db_tasks = Task.objects.all()		
		for db_task in db_tasks:
			task_name = str(db_task.task_name)			
			for task in tasks:
				if task['name'] in task_name:
					continue
				else:
					Task.objects.filter(task_name=task_name).delete()