import asana

class AsanaSession:

	me = None
	client = None

	def check_token(token):
		
		success = False
		try:
			AsanaSession.client = asana.Client.access_token(token)
			AsanaSession.me = AsanaSession.client.users.me()
			success = True
		except asana.error.NoAuthorizationError:
			success = False

		return success

	def projects_list(token):
		projects = []		
		AsanaSession.check_token(token)
		if AsanaSession.me is not None:
			ws_id = AsanaSession.me['workspaces'][0]['id']
			for project in AsanaSession.client.projects.find_all({'workspace': ws_id}):	
				info = {
					'id': project['id'],
					'name': project['name']
				}
				projects.append(info)

		return projects

	def tasks_list(task_id, token):
		tasks = []
		AsanaSession.check_token(token)				
		if AsanaSession.me is not None:			
			try:				
				for task in AsanaSession.client.tasks.find_all({'project': task_id}):								
					if not ":" in task['name']:
						tasks.append(task)
			except asana.error.InvalidRequestError:
				print("Error with project: " + str(task_id))		
		return tasks