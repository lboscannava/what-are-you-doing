import requests, json, re

class EverhourSession:
	base_url = 'https://everhour.com/api/v1'	

	pattern = '\s(([0-9]+\.[0-9]+)|([0-9]+))(hrs|hr|h|min|m| hrs| hr| h| min| m)|([0-9]+)(a.m.|p.m.| a.m.| p.m.) till ([0-9]+)(a.m.|p.m.| a.m.| p.m.)'

	def check_api_key(api_key):
		return requests.get(EverhourSession.base_url + '/auth/validateAccessToken/', params= {'accessToken': api_key})

	def create_time_entry(api_key, comment, today):
			data = {'entry': comment, 'today': today}
			header = {'content-type': 'application/json'}

			if(re.search(EverhourSession.pattern, comment) is None):
				requests.get(EverhourSession.base_url + '/timer/stop/', params= {'accessToken': api_key})
			track = requests.put(EverhourSession.base_url + '/time_entries/?accessToken=' + api_key, data=json.dumps(data), headers=header)
			if(re.search(EverhourSession.pattern, comment) is None):
				requests.get(EverhourSession.base_url + '/timer/continue/' + str(track.json()['id']) + '/', params= {'accessToken': api_key})

	def projects_list(api_key):
		result = requests.get(EverhourSession.base_url + '/repositories/fetch/', params={'type': 'asana', 'accessToken': api_key})				
		projects = []
		for jobject in result.json():
			info = {
				'id': result.json()[jobject]['repository']['repository_id'],
				'name': result.json()[jobject]['mention']['name']
			}			
			projects.append(info)
		return projects

	def get_current_time_entry(api_key):
		result = requests.get(EverhourSession.base_url + '/timer/current/', params={'accessToken': api_key})
		return result

	def stop_timer(api_key):		
		q = requests.get(EverhourSession.base_url + '/timer/stop/', params={'accessToken': api_key})
		return q

	def start_entry(api_key, project_id):
		requests.get(EverhourSession.base_url + '/timer/continue/' + str(project_id) + "/", params={'accessToken': api_key})		