from django.contrib import admin

from .models import Post, UakaUser, Task, Project, Standup


class PostAdmin(admin.ModelAdmin):
    fields = ['content', 'user']
    list_filter = ['user']


class StandupAdmin(admin.ModelAdmin):
    list_filter = ['user', 'date']

admin.site.register(Post, PostAdmin)
admin.site.register(UakaUser)
admin.site.register(Project)
admin.site.register(Task)
admin.site.register(Standup, StandupAdmin)
