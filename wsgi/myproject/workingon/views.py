from django.contrib.auth.decorators import login_required
from django.shortcuts import render, render_to_response, redirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.template import RequestContext
from django.utils.translation import ugettext as _
from endless_pagination.decorators import page_template
from .models import *
from .forms import ApiKeyForm, StandupForm, DateRangeForm
from .everhourapi import EverhourSession
from .asanaapi import AsanaSession
from django.utils import translation
import datetime
import json
import math


@login_required(login_url='/login/')
def post_view(request):

    if UakaUser.objects.filter(user=request.user):
        translation.activate(
            str(UakaUser.objects.filter(user=request.user)[0].language))

    user = check_user(request)

    is_last = False

    if user.api_key:
        is_last = check_last_entry(request, user)

    status = 0

    last_post = Post.objects.filter(user=request.user).order_by('-date')[:1]
    form = ApiKeyForm()

    if request.is_ajax():
        if request.method == 'GET':
            action = request.GET.get('action')
            stop_play_validation(request, action, user)
        elif request.method == 'POST':
            api_key = request.POST.get('api_key')
            token = request.POST.get('token')

            if EverhourSession.check_api_key(api_key).status_code == 200 and AsanaSession.check_token(token):
                user.api_key = api_key
                user.access_token = token
                user.save()
                update_projects(user)
                is_last = check_last_entry(request, user)
                status = 200
            else:
                status = 500
            return HttpResponse(
                json.dumps({'status': status}),
                content_type="application/json")
    elif request.method == 'POST':
        tmp_content = request.POST.get('new-post')
        if tmp_content and tmp_content != "":
            current_time = timezone.localtime(timezone.now())
            request.user.post_set.create(
                content=tmp_content, date=current_time)
            request.user.save()
            if user.api_key:
                EverhourSession.create_time_entry(
                    user.api_key, tmp_content, current_time.isoformat())
                is_last = check_last_entry(request, user)
            return HttpResponseRedirect(reverse('index'))

    (current_standup, last_standup, standup_form) = check_standup(request.user)

    context = {
        'last_post': last_post,
        'title': _('Hey! ') + request.user.username.title() + _(' What are you doing?'),
        'api_key': user.api_key,
        'token': user.access_token,
        'form': form,
        'standup_form': standup_form,
        'last_standup': last_standup,
        'current_standup': current_standup,
        'is_last': is_last
    }

    request.session['view'] = 'index'
    request.session['args'] = None
    return render(request, 'workingon/home.html', context)


@login_required(login_url='/login/')
def standups_list_view(request):
    users = User.objects.order_by('username')
    date = datetime.date.today()
    day = date - datetime.timedelta(date.weekday())
    form = DateRangeForm()
    week_days = []

    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')

    if start_date and end_date:
        start_date = datetime.datetime.strptime(start_date, "%m/%d/%Y").date()
        end_date = datetime.datetime.strptime(end_date, "%m/%d/%Y").date()
        limit = int(math.fabs((start_date - end_date).days))
        if limit > 7:
            limit = 7
        if start_date > end_date:
            for i in range(0, limit + 1):
                week_days.append(end_date + datetime.timedelta(i))
        elif start_date < end_date:
            for i in range(0, limit + 1):
                week_days.append(start_date + datetime.timedelta(i))
        elif start_date == end_date:
            week_days.append(start_date)
    else:
        for i in range(0, 7):
            week_days.append(day + datetime.timedelta(i))

    table = create_table(users, week_days)

    context = {
        'week_days': week_days,
        'title': 'Stand ups',
        'table': table,
        'form': form
    }

    return render(request, 'workingon/standups.html', context)


@login_required(login_url='/login/')
def standup_view(request):
    if request.is_ajax():
        if request.method == 'POST':
            standup = request.POST.get('standup_today')
            if standup and standup != "":
                Standup.objects.create(user=request.user, today=standup)
                return HttpResponse(
                    json.dumps({'today': standup}),
                    content_type="application/json")
    else:
        return HttpResponseRedirect(reverse('index'))
    return render(request, 'workingon/home.html')


@login_required(login_url='/login/')
def update_standup_view(request):
    new_value = request.GET.get('new_value')
    today = timezone.localtime(timezone.now()).date()
    try:
        if new_value != "Empty" and new_value != "Vacío":
            standup = Standup.objects.filter(user=request.user,
                                             date=today).update(today=new_value)
    except Error:
        print("Error updating standup!")


@login_required(login_url='/login/')
def update_details_view(request):
    new_value = request.GET.get('new_value')
    today = timezone.localtime(timezone.now()).date()
    try:
        if new_value != "Empty" and new_value != "Vacío":
            standup = Standup.objects.filter(user=request.user,
                                             date=today).update(details=new_value)
    except Error:
        print("Error updating details!")


@login_required(login_url='/login/')
def settings_view(request):
    if UakaUser.objects.filter(user=request.user):
        translation.activate(
            str(UakaUser.objects.filter(user=request.user)[0].language))

    context = {
        'title': _('Settings')
    }
    request.session['view'] = 'settings'
    return render(request, 'workingon/setting.html', context)


def login_view(request):
    return render(request, 'workingon/login.html')


@login_required(login_url='/login/')
def all_posts_view(request):

    if UakaUser.objects.filter(user=request.user):
        translation.activate(
            str(UakaUser.objects.filter(user=request.user)[0].language))

    latest_posts_all_users = []

    for u in User.objects.all():
        if Post.objects.filter(user=u).last():
            latest_posts_all_users.append(Post.objects.filter(user=u).last())

    if latest_posts_all_users is not None:
        latest_posts_all_users = sorted(
            latest_posts_all_users, key=lambda x: x.date, reverse=True)

    context = {
        'latest_posts_all_users': latest_posts_all_users,
        'title': _("Everybody's Last Activities")
    }

    request.session['view'] = 'all-posts'
    request.session['args'] = None
    return render(request, 'workingon/records.html', context)


@login_required(login_url='/login/')
def user_posts(request, name, extra_context=None):

    if UakaUser.objects.filter(user=request.user):
        translation.activate(
            str(UakaUser.objects.filter(user=request.user)[0].language))

    current_user = User.objects.filter(username=name)[0]

    latest_posts = Post.objects.filter(user=current_user).order_by('-date')
    context = {
        'latest_posts': latest_posts,
        'current_user': current_user,
        'title': current_user.email + _(' activities')
    }

    if extra_context is not None:
        context.update(extra_context)

    request.session['view'] = 'user-posts'
    request.session['args'] = str(current_user)
    return render_to_response('workingon/record-user.html', context, context_instance=RequestContext(request))


def logout_view(request):
    logout(request)
    return redirect('login')


def check_user(request):

    exist = UakaUser.objects.filter(user=request.user)

    if not exist:
        new_user = UakaUser(language='es', user=request.user)
        new_user.save()

    return new_user if not exist else exist[0]


def activate_lang(request, lang):
    translation.activate(str(lang))

    view = request.session.get('view')
    args = request.session.get('args')

    uakauser = UakaUser.objects.filter(user=request.user)[0]
    uakauser.language = lang
    uakauser.save()
    if args:
        return redirect(view, name=args)
    else:
        return redirect(view)


def update_projects(user):
    projects = EverhourSession.projects_list(user.api_key)
    test = AsanaSession.projects_list(user.access_token)
    for project in projects:
        tmp = Project.objects.filter(project_name="@" + project['name'])
        if len(tmp) is 0:
            new_project = Project.objects.create(
                project_name="@" + project['name'], project_id=project['id'])
            new_project.save()
            update_tasks(new_project, project['id'], user.access_token)
        else:
            update_tasks(tmp[0], tmp[0].project_id, user.access_token)


def update_tasks(project, task_id, token):
    tasks = AsanaSession.tasks_list(str(task_id), token)
    for task in tasks:
        tmp = Task.objects.filter(project=project, task_name=task['name'])
        if len(tmp) is 0:
            new_task = Task.objects.create(
                project=project, task_name=task['name'])
            new_task.save()


def check_last_entry(request, user):
    entry = EverhourSession.get_current_time_entry(user.api_key)
    post = None
    if entry:
        if entry.text == 'null':
            return False
        else:
            post = Post.objects.last()
            request.session['stoped_project_id'] = entry.json()['id']
            return (post.content == entry.json()['comment'] or entry.json()['comment'] in post.content)
    else:
        return False


def check_standup(user):
    today = timezone.localtime(timezone.now()).date()
    yesterday = today - datetime.timedelta(days=1)
    form = StandupForm()

    try:
        current_standup = Standup.objects.filter(user=user, date=today)[0]
    except IndexError:
        current_standup = None

    try:
        last_standup = Standup.objects.filter(user=user, date=yesterday)[0]
    except IndexError:
        last_standup = None

    if current_standup:
        form.fields['today'].widget.attrs['disabled'] = True

    return (current_standup, last_standup, form)


def stop_play_validation(request, action, user):
    if action == "play":
        project_id = request.session['stoped_project_id']
        EverhourSession.start_entry(user.api_key, project_id)
    elif action == "stop":
        if user.api_key:
            result = EverhourSession.stop_timer(user.api_key)
            if result and result.text != 'null':
                request.session['stoped_project_id'] = result.json()['id']


def create_table(users, week_days):
    table = []
    empty = True
    for user in users:
        temp = []
        if user.standups.count() > 0:
            temp.append(user.username)
            for day in week_days:
                for st in user.standups.all():
                    if day == st.date:
                        temp.append(st)
                        empty = False
                        break
                    elif st == user.standups.last():
                        temp.append(None)
        if not empty:
            table.append(temp)
            empty = True

    if len(table) == 0:
        return None

    return table
