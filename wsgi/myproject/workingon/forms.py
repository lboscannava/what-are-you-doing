from django import forms
from workingon.models import Standup
from functools import partial

DateInput = partial(forms.DateInput, {'class': 'datepicker'})


class ApiKeyForm(forms.Form):
    text_everhour = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'id': 'api-text', 'required': True, 'placeholder': 'Everhour API Key'}))
    text_asana = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'id': 'token-text', 'required': True, 'placeholder': 'Asana Access Token'}))


class StandupForm(forms.models.ModelForm):

    class Meta:
        model = Standup
        fields = ('today',)
        widgets = {
            'today': forms.fields.TextInput(attrs={
                'placeholder': 'Write your standup',
                'id': 'standup-input',
                'aria-describedby': 'sizing-addon1',
            }),
        }

        error_messages = {
            'text': {'required': "You can't have an empty standup"}
        }


class DateRangeForm(forms.Form):
    start_date = forms.DateField(widget=DateInput())
    end_date = forms.DateField(widget=DateInput())
