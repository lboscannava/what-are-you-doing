$('#standups-list').DataTable({
    'paging': false,
    'info': false
});

$(".datepicker").datepicker({    
    maxDate: 0
});

$("#id_end_date, #id_start_date").on('change', function() {
    if ($("#id_start_date").val() !== '' && $("#id_end_date").val() !== '') {
        $(".btn-filter").attr('disabled', false);
    } else {
        $(".btn-filter").attr('disabled', true);
    }
});

if ($("#id_start_date").val() !== '' && $("#id_end_date").val() !== '') {
    $(".btn-filter").attr('disabled', false);    
} else {
    $(".btn-filter").attr('disabled', true);    
}

$("#id_start_date").on('change', function() {
    var date = $(this).datepicker('getDate');
    var minDate = new Date(date.getFullYear(), date.getMonth(), date.getDay() - 6); 
    $("#id_end_date").datepicker('option', 'minDate', minDate);
});