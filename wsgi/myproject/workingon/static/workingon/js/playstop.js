$("#play").click(function() {	
	play_validation();
});

$("#stop").click(function() {	
	stop_validation();    
});

function play_validation() {	
	 $.ajax({
            url : "/", // the endpoint
            type : "GET", // http method
            data : { action : "play" }, // data sent with the post request
            // handle a successful response
            success : function(json) {
            	$("#play").prop('disabled', true);
            	$("#stop").prop('disabled', false);
            },
            // handle a non-successful response
            error : function(xhr,errmsg,err) {
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console                
            }
        });
}

function stop_validation() {	
	 $.ajax({
            url : "/", // the endpoint
            type : "GET", // http method
            data : { action : "stop" }, // data sent with the post request
            // handle a successful response
            success : function(json) {
            	$("#play").prop('disabled', false);
            	$("#stop").prop('disabled', true);
            },
            // handle a non-successful response
            error : function(xhr,errmsg,err) {
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console                
            }
        });
}