if (typeof jQuery === 'undefined') {
	throw new Error('Editable Input JavaScript requires jQuery');
}

$(".fake-layer").width($(".editable input").width());

$(".fake-layer, .fake-layer-details").hover(function() {	
	if ($("#standup-input").attr("disabled") !== undefined || $("#standup-input").attr("disabled") === true) {
		var $editable = $(this).prev();		
		$editable.css("font-weight", "600");
	}
}, function() {
	if ($("#standup-input").attr("disabled") !== undefined || $("#standup-input").attr("disabled") === true) {
		var $editable = $(this).prev();
		$editable.css("font-weight", "100");
	}
});

$(".fake-layer, .fake-layer-details").click(function() {
	var $editable = $(this).prev();
	var $cancel = $(this).next();
	var $check = $(this).next().next();

	if ($("#standup-input").attr("disabled") !== undefined || $("#standup-input").attr("disabled") === true) {
		if ($editable.attr("disabled") !== false) {
			$(this).hide();
			$editable.attr("disabled", false);
			var text = $editable.attr("placeholder");
			$editable.val(text);
			$editable.css("border", "1px solid black");
			$editable.css("cursor", "default");
			$editable.css("background-color", "white");
			$editable.toggleClass("editable-box");
			$check.show();
			$cancel.show();
		}
	}
});

$(".cancel-button").click(function(e) {
	e.preventDefault();

	var $editable = $(this).prev().prev();
	var $fake = $(this).prev();
	var $cancel = $(this);
	var $check = $(this).next();
	var text = $editable.attr("placeholder");

	$fake.show();
	$editable.attr("disabled", true);
	$editable.attr("placeholder", text);
	$editable.val("");
	$editable.css("border", "0");
	$editable.css("cursor", "pointer");
	$editable.css("background-color", "#F9F9F9");
	$editable.toggleClass("editable-box");
	$cancel.hide();
	$check.hide();
});

$(".check-button").click(function(e) {
	e.preventDefault();

	var $editable = $(this).prev().prev().prev();
	var $fake = $(this).prev().prev();
	var $cancel = $(this).prev();
	var $check = $(this);
	var text = $editable.val();
	
	if(text !== '') {
		updateStandup($editable);

		$fake.show();
		$editable.attr("disabled", true);
		$editable.attr("placeholder", text);
		$editable.val("");
		$editable.css("border", "0");
		$editable.css("cursor", "pointer");
		$editable.css("background-color", "#F9F9F9");
		$editable.toggleClass("editable-box");
		$check.hide();
		$cancel.hide();
	}
});

var updateStandup = function(el) {
	var id = el.parent().parent().prop('id');
	var value = el.val();

	if (id === 'today') {
		$.ajax({
			url: 'update-standup/',
			data: {
				new_value: value
			}
		});
	} else {
		$.ajax({
			url: 'update-details/',
			data: {
				new_value: value
			}
		});
	}
};