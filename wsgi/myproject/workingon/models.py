from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


class UakaUser(models.Model):
    api_key = models.CharField(max_length=100, blank=True)
    access_token = models.CharField(max_length=100, blank=True)
    language = models.CharField(max_length=50)
    user = models.OneToOneField(User)

    def __str__(self):
        return self.user.first_name


class Post(models.Model):
    content = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)

    def __str__(self):
        return self.content


class Project(models.Model):
    project_id = models.BigIntegerField()
    project_name = models.CharField(max_length=500)


class Task(models.Model):
    task_name = models.CharField(max_length=1000)
    project = models.ForeignKey(Project)


class Standup(models.Model):
    today = models.CharField(max_length=200)
    details = models.CharField(max_length=100, blank=True)
    date = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, related_name='standups')

    def __str__(self):
        return self.today

    def save(self, *args, **kwargs):
        repeated_date = Standup.objects.filter(date=self.date)
        if repeated_date.count() == 0:
            super(Standup, self).save(*args, **kwargs)
        else:
            raise ValidationError('Date repeated!')

    class Meta:
        ordering = ['date']
