# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('workingon', '0011_auto_20151118_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='standup',
            name='user',
            field=models.ForeignKey(default=24, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
