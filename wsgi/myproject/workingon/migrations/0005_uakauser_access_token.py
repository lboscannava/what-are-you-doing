# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workingon', '0004_auto_20150824_1423'),
    ]

    operations = [
        migrations.AddField(
            model_name='uakauser',
            name='access_token',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
