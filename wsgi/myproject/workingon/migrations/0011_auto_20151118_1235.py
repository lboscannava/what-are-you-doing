# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workingon', '0010_standup'),
    ]

    operations = [
        migrations.AlterField(
            model_name='standup',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
