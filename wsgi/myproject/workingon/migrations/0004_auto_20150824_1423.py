# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workingon', '0003_auto_20150820_1713'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uakauser',
            name='api_key',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
