from django.conf.urls import patterns, include, url
from django.contrib import admin

from workingon import views

urlpatterns = patterns('',
                       url(r'^$', views.post_view, name="index"),
                       url(r'^standup/', views.standup_view, name="standup"),
                       url(r'^standups/', views.standups_list_view, name="standups"),
                       url(r'^update-standup/', views.update_standup_view,
                           name="update-standup"),
                       url(r'^update-details/', views.update_details_view,
                           name="update-details"),
                       url(r'^change/(?P<lang>[\w ]+)/',
                           views.activate_lang, name="lang-change"),
                       url(r'^user-posts/(?P<name>[\w ]+)/',
                           views.user_posts, name="user-posts"),
                       url(r'^all-posts/', views.all_posts_view, name="all-posts"),
                       url(r'^settings/', views.settings_view, name="settings"),
                       url(r'^login/', views.login_view, name="login"),
                       url(r'^logout/', views.logout_view, name="logout"),
                       url(r'^accounts/', include('allauth.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^myautocomplete/', include('autocomplete.urls')),
                       url(r'^autocomplete/', include('autocomplete_light.urls')),
                       )
