from django import shortcuts
from workingon.asanaapi import AsanaSession
from workingon.models import Task, Project

def autocomplete(request,
    template_name='autocomplete/autocomplete.html'):
    
    q = request.GET.get('q', '')

    context = {'q': q}
    queries = {}
    if not " " in q: 
        queries['projects'] = Project.objects.filter(project_name__icontains=q).distinct()[:6]
    elif q[0] == "@":
        project = Project.objects.filter(project_name__icontains=q[1:q.index(" ")])        
        task_name = q[q.index(" ") + 1:]
        print(task_name)
        queries['tasks'] = Task.objects.filter(project=project, task_name__icontains=task_name).distinct()[:8]

    context.update(queries)

    return shortcuts.render(request, template_name, context)
