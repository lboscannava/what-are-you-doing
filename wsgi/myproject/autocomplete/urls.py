from django.conf.urls import patterns, url

from autocomplete.views import autocomplete

urlpatterns = [
    url(r'^$', autocomplete, name='autocomplete'),
]