��          �   %   �      p     q     �  "   �     �     �     �     �     �  	   �                    #     ,     J  !   ^     �     �     �     �     �  !   �     �  /     ,   H  ,   u     �  k  �          8  1   E     w     ~     �     �     �     �     �     �     �     �  "         #      7     X     ]     e     }     �      �      �  (   �  ,      0   M     ~        
                     	                                                                                               What are you doing?  activities Click here to add your access keys Close Data Synchronized! Date Everybody's Last Activities Invalid Data! No Record Nothing published Post Send Settings Sign in - What are you doing? Sign in with Google Synchronize with Everhour & Asana Time User What are you doing? Write whatever you're doing You can find it by visiting You can find it by visiting your  You have not published any post You have not synchronized with Everhour & Asana You need to create an access token in Asana. You need your Everhour API Key to authorize. profile settings Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-17 11:57-0430
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  ¿Qué estás haciendo?  actividades Haga clic aquí para añadir sus claves de acceso Cerrar Datos Sincronizados! Fecha Últimas actividades de todos Datos inválidos! Ningún registro Nada publicado Mensaje Enviar Ajustes Ingresar - ¿Qué estás haciendo? Ingresar con Google Sincronizar con Everhour y Asana Hora Usuario ¿Qué estás haciendo? Escribe lo que estás haciendo Puedes hacerlo visitando Puedes encontrarla visitando tu  No has publicado ningún mensaje No has sincronizado con Everhour y Asana Necesitas crear un token de acceso en Asana. Necesitas tu API Key de Everhour para autorizar. perfil en Everhour 